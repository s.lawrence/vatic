.. vatic documentation master file, created by
   sphinx-quickstart on Sun Jun 26 01:30:32 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to vatic's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   cli
   library
   reference


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
