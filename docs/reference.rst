Reference
=========

Submodules
----------

vatic.kalshi module
-------------------

.. automodule:: vatic.kalshi
   :members:
   :undoc-members:
   :show-inheritance:

vatic.manifold module
---------------------

.. automodule:: vatic.manifold
   :members:
   :undoc-members:
   :show-inheritance:

vatic.metaculus module
----------------------

.. automodule:: vatic.metaculus
   :members:
   :undoc-members:
   :show-inheritance:

vatic.predictit module
----------------------

.. automodule:: vatic.predictit
   :members:
   :undoc-members:
   :show-inheritance:


